<!DOCTYPE html>
<html>
<!-- Head -->
@include('partials.head')

<body>
    <!--Nav bar-->
    @include('partials.nav_bar')

    @yield('home')
    {{-- <div class="container">
        <div class="row" id="header-bar">
            @include('partials._header_cart')
        </div>
    </div>
    <br> --}}
    <!-- Content -->
    @hasSection('content')
    <div class="main_content container page">
        @yield('content')
    </div>
    @endif

    <!-- Footer -->
    @include('partials.footer')
    @yield('scripts')
    <!-- Scripts -->
    @include('partials.scripts')
</body>

</html>
