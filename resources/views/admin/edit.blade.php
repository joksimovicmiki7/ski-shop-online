@extends('layout.admin')
@section('content')
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
  <form method="post" action="{{ route('admin.update', $product->id) }}">
            @method('PATCH')
            @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" name="name" id="name" value="{{ $product->name }}">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" class="form-control" name="price" id="price" value="{{ $product->price }}">
    </div>
    <div class="form-group">
      <label for="description">Description</label>
      <input type="text" class="form-control" id="description" name="description" value="{{ $product->description }}">
    </div>
    <img src="{{url('/uploads/products/'.$product->image)}}" style="width: 20%; height: 30%;" alt="Image"/>
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">Upload</span>
      </div>

      <div class="custom-file">
        {{-- <input type="file" class="custom-file-input" name="photo" id="photo">
        <label class="custom-file-label" for="photo">Choose file</label> --}}
        {!! Form::file('image', ['class'=>'form-control','placeholder'=>''])!!}
      </div>

    </div><br>


    {{ Form::submit('Create the Product!', ['class' => 'btn btn-primary']) }}

{{ Form::close() }}
@endsection
