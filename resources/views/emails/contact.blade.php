@extends('layout.app')
@section('content')
  <div class="container">
  <h1>Tell Us</h1>
  @if(Session::has('success'))
     <div class="alert alert-success">
       {{ Session::get('success') }}
     </div>
  @endif
  {{-- {!! Form::open(['route'=>'contact']) !!} --}}
  <form action="{{route('contact.store')}}"  class="form-group" method="post">


  @csrf
  <div class="form-control">
    <input type="text" name="name" placeholder="Your Name" value="{{ old('name')}}">
    <p>{{ $errors->first('name') }}</p>
  </div>
  <br>

  <div class="form-control">
    <input type="text" name="email" placeholder="Your Email" value="{{ old('email')}}">
    <p>{{ $errors->first('email') }}</p>
  </div>
  <br>

  <div class="form-control">
    <input type="text" name="subject" placeholder="Subject" value="{{ old('subject')}}">
    <p>{{ $errors->first('subject') }}</p>
  </div>
  <br>

  <div class="form-control">
    <textarea name="content" cols="30" rows="10" placeholder="Message">{{ old('content')}}</textarea>
    <p>{{ $errors->first('content') }}</p>
  </div>
  <br>
  <button type="submit" class="btn-primary btn">Send</button>

  {{-- {!! Form::close() !!} --}}
  </form>
  </div>
@endsection
