<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('products', 'ProductController');
Route::resource('admin', 'AdminController');
Route::resource('comment', 'CommentController');
Route::resource('contact', 'ContactController');
Route::resource('history', 'HistoryController');

Route::get('cart', 'ProductController@cart');

Route::get('add-to-cart/{id}', 'ProductController@addToCart');

Route::patch('update-cart', 'ProductController@update');

Route::delete('remove-from-cart', 'ProductController@remove');
