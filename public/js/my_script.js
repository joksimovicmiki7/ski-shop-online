// Nav bar
$(document).ready(function() {
    (function($) {
        $(".header-icon").click(function(e) {
            e.preventDefault();
            $("body").toggleClass("with-sidebar");
        });

        $(".overlay").click(function(e) {
            $("body").removeClass("with-sidebar");
        });
    })(jQuery);
});
//Pointer za klik na sliku u detaljima
$('.js-img-detail').css('cursor', 'pointer');
//Pointer za klik na proizvod
$('.js-product-detail').css('cursor', 'pointer');

//Change images on products detail
$('.js-img-detail img').click(function() {
    var obj = this;
    var srcImg = $(obj).attr('src');
    $(".js-img-show").attr("src", srcImg);
});
//Slider

wow = new WOW({
    animateClass: 'animated',
    offset: 100,
    callback: function(box) {}
});
wow.init();
document.getElementById('moar').onclick = function() {
    var section = document.createElement('section');
    section.className = 'section--purple wow fadeInDown';
    this.parentNode.insertBefore(section, this);
};