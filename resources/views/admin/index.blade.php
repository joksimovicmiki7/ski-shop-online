@extends('layout.admin')
@section('content')

<div>
  <a style="margin: 19px;" class="btn btn-primary" href="{{ route('admin.create') }}">New Product</a>
</div>
<div class="col-sm-12">

@if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div>
@endif
</div>
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Detail</th>
        <td colspan=2>Actions</td>
      </tr>
    </thead>
    <tbody>
      @foreach($products as $data)
      <tr class="products_name text-center">
        <td scope="row">{{ $data->id }}</td>
        <td>{{ $data->name }}</td>
        <td class="products_price text-center">{{ $data->price }}</td>
        <td>{{ $data->description }}</td>
        <td><a href="{{ URL::to('admin/' . $data->id) }}"><img src="{{url('/uploads/products/'.$data->image)}}" style="width: 20%; height: 30%;" alt="Image"/></a></td>
        <td>
                <a href="{{ route('admin.edit',$data->id)}}" class="btn btn-primary">Edit</a>
        </td>
        <td>
                <form action="{{ route('admin.destroy', $data->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
        </td>
      </tr>
        @endforeach
    </tbody>
  </table>

@endsection
