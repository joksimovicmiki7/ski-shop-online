<!DOCTYPE html>
<html>
<!-- Head -->
@include('partials.admin-head')

<body>
    <!--Nav bar-->
    @include('partials.admin-nav_bar')

    @yield('home')

    <!-- Content -->
    @hasSection('content')
      @if ($errors->any())
       <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
           </ul>
       </div>
       <br />
     @endif
    <div class="main_content container">
        @yield('content')
    </div>
    @endif

    <!-- Footer -->
    @include('partials.admin-footer')

    <!-- Scripts -->
    @include('partials.scripts')
</body>

</html>
