
    <div class="col-md-3 col-sm-6">
        <!-- Images -->
        <div class="products_image">
            <a href="/products/{id}"><img class="img-fluid js-product-detail" src="{{url('/images/1.jpg')}}"></a>
        </div>
        <!-- linija -->
        <div class="products_line"></div>
        <!-- Info -->
        <div class="products_info ">
            <p class="products_name text-center">{{ $products->name }}</p>
            <p class="products_price text-center text-warning">{{ $products->price }}</p>
            <p class="text-center">
              <button type="button" class="btn btn-danger ">Dodaj u korpu</button>
            </p>
        </div>
    </div>
