@extends('layout.app')
@section('home')
<!-- Slider -->
<section>
    <div id="slider-animation" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#slider-animation" data-slide-to="0" class="active"></li>
    <li data-target="#slider-animation" data-slide-to="1"></li>
    <li data-target="#slider-animation" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner slider_img">
    <div class="carousel-item active ">
      <img src="{{ url('/images/slider1.jpg') }}" alt="Los Angeles">
        <div class="text-box">
            <h2 class="wow slideInRight" data-wow-duration="2s">This is Obitope text</h2>
            <p class="wow slideInLeft" data-wow-duration="2s">There is now an abundance of readable dummy texts. These are usually used when a text is required purely to fill a space. </p>
        </div>
    </div>
    <div class="carousel-item">
      <img src="{{ url('/images/slider3.jpg') }}" alt="Chicago">
    <div class="text-box">
            <h2 class="wow slideInUp" data-wow-duration="4s" >This is samuel text</h2>
            <p class="wow fadeInDown" data-wow-duration="4s">There is now an abundance of readable dummy texts. These are usually used when a text is required purely to fill a space. </p>
        </div>
    </div>
    <div class="carousel-item">
      <img src="{{ url('/images/slider2.jpg') }}" alt="New York">
    <div class="text-box">
            <h2 class="wow fadeInUp" data-wow-duration="4s">This is Airborne text</h2>
            <p class="wow fadeInUp" data-wow-duration="2s">There is now an abundance of readable dummy texts. These are usually used when a text is required purely to fill a space. </p>
        </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#slider-animation" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#slider-animation" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>

</section>


@endsection
