<div class="row cart_thumb">
        <!-- Images -->
    <div class="col-md-3 col-xs-3 col-sm-3 cart_image">
            <img class="img-fluid" src="{{url('/images/1.jpg')}}">

    </div>
    <!-- Name -->
    <div class="cart_name col-md-4 col-xs-4 col-sm-4">
        <h3>Skije Atomic</h3>
    </div>
    <!-- Cena -->
    <div class="cart_price col-md-4 col-xs-4 col-sm-4">
        <h3>422 EUR</h3>
    </div>
    <div class="col-md-1 col-xs-1 col-sm-1"></div>
</div>