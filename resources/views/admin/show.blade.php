@extends('layout.admin')
@section('content')
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
<div class="row products_detail_div">
    <!-- Products info -->
    <div class="col-5">
            <!-- Naziv -->
            <h1 class="text-left">{{ $product->name }}</h1>
            <!-- Cena -->
            <p class="text-left">Cena: </p>
            <h3 class="text-left text-warning">{{ $product->price }}</h3>
            <p class="text-left">Opis: </p>
            <p class="text-center">{{ $product->description }}</p>
            <p class="text-left">Slika: </p>
            <p class="text-left"><img src="{{url('/uploads/products/'.$product->image)}}" style="width: 100%; height: 100%;" alt="Image"/></p>
            <!-- linija -->
            <div class="products_line"></div>

    </div>

</div>

<div class="products_comments_div row">
        <div class="col-md-6 col-xs-12">
                <div class="row">
                        <i class="fa fa-comments-o" aria-hidden="true" style="font-size: 200%;"></i>
                        <h3 class="float-left">Comments</h3>
                </div>
                @foreach($comments as $comment)
                  <div class=" shadow-sm product_comment_thumb">
                      <div class="row">
                          <!-- Ime -->
                          <div class="col-6">
                              <p class="font-weight-bold">{{ $comment->name }}</p>
                          </div>
                          <!-- Datum -->
                          <div class="col-6">
                              <p class="text-right">{{ $comment->created_at }}</p>
                          </div>
                          <!-- Comentar -->
                          <div class="col-12 products_comment_text">
                          <p class="text-justify font-weight-light ">{{ $comment->comment }}</p>
                          </div>
                      </div>
                  </div>
                @endforeach
        </div>
  </div>
@endsection
