@extends('layout.admin')
@section('content')
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
{{ Form::open(['route' => 'admin.store', 'files' => 'true']) }}
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" class="form-control" name="price" id="price" placeholder="Enter Price">
    </div>
    <div class="form-group">
      <label for="description">Description</label>
      <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description">
    </div>

    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">Upload</span>
      </div>

      <div class="custom-file">
        {{-- <input type="file" class="custom-file-input" name="photo" id="photo">
        <label class="custom-file-label" for="photo">Choose file</label> --}}
        {!! Form::file('image', ['class'=>'form-control','placeholder'=>''])!!}
      </div>

    </div><br>


    {{ Form::submit('Create the Product!', ['class' => 'btn btn-primary']) }}
    {{ csrf_field() }}
{{ Form::close() }}
@endsection
