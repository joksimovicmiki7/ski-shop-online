<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Image;
use App\Comment;

class AdminController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Product::all();

      return view('admin.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([

         'image'        =>'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
     ]);
     $product = new Product();
     $product->name = $request->get('name');
     $product->price = $request->get('price');
     $product->description = $request->get('description');
     if ($request->hasFile('image')) {
       $image = $request->file('image');
       $name = $request->name.'.'.$image->getClientOriginalExtension();
       $destinationPath = public_path('/uploads/products');
       $imagePath = $destinationPath. "/".  $name;
       $image->move($destinationPath, $name);
       $product->image = $name;
     }


     // $product = new Product([
     //     'name'         => $request->get('name'),
     //     'price'        => $request->get('price'),
     //     'description'  => $request->get('description'),
     //     'image'        => $path
     // ]);
     $product->save();
     // return view('admin');
         return view('admin.create')->with('success', 'Your product has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // get the product
      $product = Product::where('id', $id)->firstOrFail();
      $comments = Comment::all()->where('product_id', $id);

      // show the view and pass the product to it
      return view('admin.show', compact('comments'))
          ->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // get the product
      $product = Product::find($id);

      // show the edit form and pass the product
      return view('admin.edit')
          ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $request->validate(['image' =>'required|file|image|mimes:jpeg,png,gif,webp|max:2048']);
       $product = Product::find($id);
       $product = new Product();
       $product->name = $request->get('name');
       $product->price = $request->get('price');
       $product->description = $request->get('description');
       if ($request->hasFile('image')) {
         $image = $request->file('image');
         $name = $request->name.'.'.$image->getClientOriginalExtension();
         $destinationPath = public_path('/uploads/products');
         $imagePath = $destinationPath. "/".  $name;
         $image->move($destinationPath, $name);
         $product->image = $name;
       }


     // $product = new Product([
     //     'name'         => $request->get('name'),
     //     'price'        => $request->get('price'),
     //     'description'  => $request->get('description'),
     //     'image'        => $path
     // ]);
     $product->save();
     // return view('admin');
         return view('admin.index')->with('success', 'Your product has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Get and Delete
        $product = Product::find($id);
        $product->delete();

        //Redirect after with a message
        return back()->with('success', 'Product deleted!');
    }
}
