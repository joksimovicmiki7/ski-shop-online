@extends('layout.app')
@section('content')

<div class="row products_detail_div">
        <!-- Small images -->
        <div class="col-2 js-img-detail">
            {{-- <img src="{{url('/uploads/products/'.$product->image)}}"  style="width: 50%; height: 50%;"/> --}}
        </div>
        <!-- Large images -->
        <div class="col-5">
            <img src="{{url('/uploads/products/'.$product->image)}}" style="width: 100%; height: 100%;"/>
        </div>
        <!-- Products info ************** -->
        <div class="col-5">
                <!-- Naziv -->
                <h1 class="text-left">{{ $product->name }}</h1>
                <!-- Cena -->
                <p class="text-left">Cena: </p>
                <h3 class="text-left text-warning  ">{{ $product->price }}</h3>
                <p class="text-left">Opis: </p>
                <p class="text-center">{{ $product->description }}</p>
                <!-- linija -->
                <div class="products_line"></div>
                <button type="button" class="btn btn-danger float-right">Add to Cart</button>
        </div>
</div>

<!-- Products Comment -->
<div class="products_comments_div row">
        <div class="col-md-6 col-xs-12">
                <div class="row">
                        <i class="fa fa-comments-o" aria-hidden="true" style="font-size: 200%;"></i>
                        <h3 class="float-left">Comments</h3>
                </div>
                @foreach($comments as $comment)
                  <div class=" shadow-sm product_comment_thumb">
                      <div class="row">
                          <!-- Ime -->
                          <div class="col-6">
                              <p class="font-weight-bold">{{ $comment->name }}</p>
                          </div>
                          <!-- Datum -->
                          <div class="col-6">
                              <p class="text-right">{{ $comment->created_at }}</p>
                          </div>
                          <!-- Comentar -->
                          <div class="col-12 products_comment_text">
                          <p class="text-justify font-weight-light ">{{ $comment->comment }}</p>
                          </div>
                      </div>
                  </div>
                @endforeach
        </div>
        <!-- Add comment -->
        <div class="col-md-6 col-sm-6 ">
                <div class="col-12 " style="height: 50px;">
                        <button class="btn  btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Add Comment
                        </button>
                </div>
                <div class="collapse col-12" id="collapseExample">
                        {{-- <form> --}}
                          {{ Form::open(['url' => 'comment']) }}
                                <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" aria-describedby="TextHelp" placeholder="Enter Name">
                                        <label for="comment">Comment: </label><input type="text" class="form-control" id="comment" name="comment" aria-describedby="TextHelp" placeholder="About Product">
                                        <input type="hidden" class="form-control" id="product_id" name="product_id" value="{{ $product->id }}"aria-describedby="TextHelp" placeholder="product id">
                                </div>
                                <button type="submit" class="btn btn-primary">Comment</button>
                          {{ Form::close() }}
                        {{-- </form> --}}
                </div>
        </div>
</div>
@endsection
