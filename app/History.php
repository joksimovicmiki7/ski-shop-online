<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
  protected $fillable = [
      'name',
      'price',
      'quantity',
      'contact',
      'address',
      'product_id'
  ];
}
