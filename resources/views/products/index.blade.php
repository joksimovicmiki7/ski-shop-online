@extends('layout.app')

@section('content')

<div class="row product_main_div">
  @foreach($products as $data)
    <div class="col-md-3 col-sm-6">
        <!-- Images -->
          <div class="products_image">
              <a href="{{ URL::to('products/' . $data->id) }}">
                <img src="{{url('/uploads/products/'.$data->image)}}" style="width: 100%; height: 100%;" alt="Image"/>
              </a>
          </div>
        <!-- linija -->
        <div class="products_line"></div>
        <!-- Info -->
        <div class="products_info ">
            <p class="products_name text-center">{{ $data->name }}</p>
            <p class="products_price text-center text-warning">{{ $data->price }}</p>
            <p class="btn-holder">
              <a href="javascript:void(0);" data-id="{{ $data->id }}" class="btn btn-warning btn-block text-center add-to-cart" role="button">Add to cart</a>
                  <i class="fa fa-circle-o-notch fa-spin btn-loading" style="font-size:24px; display: none"></i>
            </p>
        </div>
    </div>
  @endforeach

</div>
{{-- <div class="container products">
        <span id="status"></span>

        <div class="row">

            @foreach($products as $product)
                <div class="col-xs-18 col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <img src="{{url('/uploads/products/'.$product->image)}}" width="250" height="250">
                        <div class="caption">
                            <h4>{{ $product->name }}</h4>
                            <p>{{ strtolower($product->description) }}</p>
                            <p><strong>Price: </strong> {{ $product->price }}$</p>
                            <p class="btn-holder">
                              <a href="javascript:void(0);" data-id="{{ $product->id }}" class="btn btn-warning btn-block text-center add-to-cart" role="button">Add to cart</a>
                                <i class="fa fa-circle-o-notch fa-spin btn-loading" style="font-size:24px; display: none"></i>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div><!-- End row -->

    </div> --}}
@endsection
@section('scripts')

    <script type="text/javascript">
        $(".add-to-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            ele.siblings('.btn-loading').show();

            $.ajax({
                url: '{{ url('add-to-cart') }}' + '/' + ele.attr("data-id"),
                method: "get",
                data: {_token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function (response) {

                    ele.siblings('.btn-loading').hide();

                    $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#header-bar").html(response.data);
                }
            });
        });
    </script>

@stop
