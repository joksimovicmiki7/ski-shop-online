<nav class="navbar navbar-expand-lg navbar-primary bg-dark">
  <a class="navbar-brand" href="{{ url('home') }}">SKISHOP</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ URL::to('products') }}">Store</a>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" href="{{ url('cart') }}">Cart</a>
      </li> --}}
      <li class="nav-item">
        <a class="nav-link " href="{{ URL::to('contact') }}">Contact</a>
      </li>
      <li class="nav_item">
        @include('partials._header_cart')
      </li>
    </ul>


    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">

    </ul>


  </div>
</nav>
